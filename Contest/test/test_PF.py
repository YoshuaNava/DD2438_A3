

import sys
from pathlib import Path 
root = str(Path(__file__).resolve().parents[1])
sys.path.append(root)
import numpy as np
import classes.ParticleFilter2D as PF
from pprint import pprint

from matplotlib import pyplot as plt






meas_csv = np.genfromtxt('../classes/PF_test_files/LAB2_mov_meas1.csv', delimiter=",")
gtruth_csv = np.genfromtxt('../classes/PF_test_files/LAB2_mov_true1.csv', delimiter=",")

num_meas = meas_csv.shape[1]
pf = PF.ParticleFilter2D(100, 0.5)
pf.set_filter_psarametersLAB2()
pf.generate_initial_belief()
for step in range(0, num_meas):
    
    print("Iteration", step)
    x_i = gtruth_csv[..., num_meas - step - 1]
    z_i = meas_csv[..., num_meas - step - 1]

    pf.predict()
    # pprint(pf.particle_states)

    pf.calculate_weights(step, z_i)

    pf.systematic_resample(step)
#    fig, ax = plt.subplots()
#    pf.plot_distribution(x_i, z_i, fig, ax)
#    plt.title('Resampling')
#    plt.show()


print("Final distribution")
fig, ax = plt.subplots()
pf.plot_distribution(x_i, z_i, fig, ax)
plt.title('Resampling')
plt.show()
