#!/bin/bash

# trap ctrl_c INT

# function ctrl_c() 
# {
#     echo "** Trapped CTRL-C"
# }



#RED=playing_modes/doNothing.py
# RED=playing_modes/ghostAI.py
#RED=playing_modes/baselineTeam.py
# RED=playing_modes/strongBaselineTeam.py
# RED=playing_modes/ghostbustersAI_Abril14.py
#RED=playing_modes/Semifinals/alphaTroll_py.py
#RED=playing_modes/Semifinals/PacManInTheShell.py
#RED=playing_modes/Semifinals/NicNacPacWack.py
#RED=playing_modes/Semifinals/PacYourFood.py
#RED=playing_modes/Semifinals/agentSmithOld.py
#RED=playing_modes/baselineTeamBetter.py
#RED=playing_modes/Finals/agentSmith_final.py
RED=playing_modes/Finals/NicNacPacWack.py
#RED=playing_modes/Finals/alphaTroll.py


# BLUE=playing_modes/doNothing.py
# BLUE=playing_modes/ghostAI.py
# BLUE=playing_modes/HannahAI2.py
#BLUE=playing_modes/strongBaselineTeam.py
#BLUE=playing_modes/Semifinals/semifinals_ghostbustersAI_Abril13.py
#BLUE=playing_modes/ghostbustersAI_Abril15_safeEnter.py
BLUE=playing_modes/ghostbustersAI_Abril16.py


#LAYOUT=tinyCapture.lay
#LAYOUT=distantCapture.lay
#LAYOUT=fastCapture.lay
#LAYOUT=bloxCapture.lay
#LAYOUT=jumboCapture.lay
#LAYOUT=defaultCapture.lay
#LAYOUT=purgatoryCapture.lay
#LAYOUT=rushCapture.lay
#LAYOUT=minotaurCapture.lay
#LAYOUT=voidCapture.lay

python capture.py -r $RED -b $BLUE -l RANDOM -n 100 -Q