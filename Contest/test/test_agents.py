#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Apr  4 17:31:02 2017

@author: alfredoso
"""

import sys
from pathlib import Path 
root = str(Path(__file__).resolve().parents[1])
sys.path.append(root)
from capture import *


"""
The main function called when pacman.py is run
from the command line:

> python capture.py

See the usage string for more details.

> python capture.py --help
"""
options = readCommand( sys.argv[1:] ) # Get game components based on input
games = runGames(**options)

save_score(games[0])

