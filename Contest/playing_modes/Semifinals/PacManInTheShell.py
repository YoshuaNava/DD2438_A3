# myTeam.py
# ---------
# Licensing Information:  You are free to use or extend these projects for
# educational purposes provided that (1) you do not distribute or publish
# solutions, (2) you retain this notice, and (3) you provide clear
# attribution to UC Berkeley, including a link to http://ai.berkeley.edu.
# 
# Attribution Information: The Pacman AI projects were developed at UC Berkeley.
# The core projects and autograders were primarily created by John DeNero
# (denero@cs.berkeley.edu) and Dan Klein (klein@cs.berkeley.edu).
# Student side autograding was added by Brad Miller, Nick Hay, and
# Pieter Abbeel (pabbeel@cs.berkeley.edu).


from captureAgents import CaptureAgent
import random, time, util
from game import Directions
import game
from util import nearestPoint

#################
# Team creation #
#################

class keep_track_of_gameState:
    # To get hold of gameState outside of CaptureAgent Classes
    def __init__(self):
        self.gameState = 0

    def get_gameState(self):
        return self.gameState

    def refresh_gameState(self, gameState):
        self.gameState = gameState

global_gamestate = keep_track_of_gameState()


def createTeam(firstIndex, secondIndex, isRed,
               first = 'DummyAgent2', second = 'DummyDummyAgent2'):
  """
  This function should return a list of two agents that will form the
  team, initialized using firstIndex and secondIndex as their agent
  index numbers.  isRed is True if the red team is being created, and
  will be False if the blue team is being created.

  As a potentially helpful development aid, this function can take
  additional string-valued keyword arguments ("first" and "second" are
  such arguments in the case of this function), which will come from
  the --redOpts and --blueOpts command-line arguments to capture.py.
  For the nightly contest, however, your team will be created without
  any extra arguments, so you should make sure that the default
  behavior is what you want for the nightly contest.
  """

  # The following line is an example only; feel free to change it.
  return [eval(first)(firstIndex), eval(second)(secondIndex)]

##########
# Agents #
##########

class DummyAgent(CaptureAgent):
  """
  A Dummy agent to serve as an example of the necessary agent structure.
  You should look at baselineTeam.py for more details about how to
  create an agent as this is the bare minimum.
  """

  def registerInitialState(self, gameState):
    """
    This method handles the initial setup of the
    agent to populate useful fields (such as what team
    we're on).

    A distanceCalculator instance caches the maze distances
    between each pair of positions, so your agents can use:
    self.distancer.getDistance(p1, p2)

    IMPORTANT: This method may run for at most 15 seconds.
    """

    '''
    Make sure you do not delete the following line. If you would like to
    use Manhattan distances instead of maze distances in order to save
    on initialization time, please take a look at
    CaptureAgent.registerInitialState in captureAgents.py.
    '''
    CaptureAgent.registerInitialState(self, gameState)

    '''
    Your initialization code goes here, if you need any.

    '''
    ## Things I've added
    self.start = gameState.getAgentPosition(self.index)
    #self.check_for_enemies = check_for_enemies() 
    #self.BT = Behavior_Tree()



  def getSuccessor(self, gameState, action):
    """
    Finds the next successor which is a grid position (location tuple).
    """
    successor = gameState.generateSuccessor(self.index, action)
    pos = successor.getAgentState(self.index).getPosition()
    if pos != nearestPoint(pos):
      # Only half a grid position was covered
      return successor.generateSuccessor(self.index, action)
    else:
      return successor

  def chooseAction(self, gameState):
    """
    Picks among actions randomly.
    """
    actions = gameState.getLegalActions(self.index)
    # Returns the food you're meant to eat. This is in the form of a matrix
    # where m[x][y]=true if there is food you can eat (based on your team) in that square.
    foods = self.getFood(gameState).asList()
    #print(foods)
    shortest = 9999
    best_action = []
    for food in foods:
        for action in actions:
            successor = self.getSuccessor(gameState, action)
            pos2 = successor.getAgentPosition(self.index)
            dist = self.getMazeDistance(pos2,food)
            if dist < shortest:
                shortest = dist
                best_action = action






    #print("HERE THEY ARE", actions)
    ## IDea for super basic tree: get legal actions, check which is closest to food, choose this.

    '''
    You should change this in your own agent.
    '''

    #util.pause()
    return best_action#random.choice(actions)

class DummyDummyAgent2(CaptureAgent):
  """
  A Dummy agent to serve as an example of the necessary agent structure.
  You should look at baselineTeam.py for more details about how to
  create an agent as this is the bare minimum.
  """

  def registerInitialState(self, gameState):
    """
    This method handles the initial setup of the
    agent to populate useful fields (such as what team
    we're on).

    A distanceCalculator instance caches the maze distances
    between each pair of positions, so your agents can use:
    self.distancer.getDistance(p1, p2)

    IMPORTANT: This method may run for at most 15 seconds.
    """

    '''
    Make sure you do not delete the following line. If you would like to
    use Manhattan distances instead of maze distances in order to save
    on initialization time, please take a look at
    CaptureAgent.registerInitialState in captureAgents.py.
    '''
    CaptureAgent.registerInitialState(self, gameState)

    '''
    Your initialization code goes here, if you need any.

    '''

    self.start = gameState.getAgentPosition(self.index)
    enemy_state = look_for_enemies(0,0,gameState, self) # If True then this returns a tuple, state(1) should be the action
    food_state = look_for_food(0,0,gameState, self)
    capsule_state = get_to_tha_capsule(0,0,gameState, self)
    self.decision_tree = Select([enemy_state, capsule_state, food_state]) # Can we send gamestate to select? This doesn't really matter


  def getSuccessor(self, gameState, action):
    """
    Finds the next successor which is a grid position (location tuple).
    """
    
    successor = gameState.generateSuccessor(self.index, action)
    pos = successor.getAgentState(self.index).getPosition()
    if pos != nearestPoint(pos):
      # Only half a grid position was covered
      return successor.generateSuccessor(self.index, action)
    else:
      return successor

  def chooseAction(self, gameState): # We must run the tree in here each call

    #state = look_for_enemies(gameState,self)
    #found, movement = state.is_enemy_close(gameState, self)
    

    # REMEMBER THAT 
    # SUCCESS = 0
    # FAIL = 1
    # RUN = 2

    global_gamestate.refresh_gameState(gameState)


    enemy_state = look_for_enemies(0,0,gameState, self) # 
    food_state = look_for_food(0,0,gameState, self)
    capsule_state = get_to_tha_capsule(0,0,gameState, self)
    self.decision_tree = Select([enemy_state, capsule_state, food_state]) # The order is important here
    
    move = 0
    for status in self.decision_tree.iterator:
        move = status   
        if status[0] == 0: 
            #print "asso vafon"
            return status[1]
        #else:
        #            move = status 
        #print move, "IS THIS SOMETHING"
        #    return self.chooseAction22(gameState)

        move = status 
        #print move, "IS THIS SOMETHING"
    self.decision_tree.reset
    #print "naha, det blev random"
    return self.chooseAction22(gameState)
    # if found:
    #     #util.pause()
    #     print "fuk shit"
    #     return movement
    #else:
    #    print "JAHAJA"
        #util.pause()
    #    return self.chooseAction22(gameState)#random.choice(actions)

  def chooseAction22(self, gameState):
    """
    Picks among actions randomly.
    """
    actions = gameState.getLegalActions(self.index)
    # Returns the food you're meant to eat. This is in the form of a matrix
    # where m[x][y]=true if there is food you can eat (based on your team) in that square.
    foods = self.getFood(gameState).asList()
    shortest = 9999
    best_action = []
    foods = foods[:1]
    for food in foods:
        for action in actions:
            successor = self.getSuccessor(gameState, action)
            pos2 = successor.getAgentPosition(self.index)
            dist = self.getMazeDistance(pos2,food)
            if dist < shortest:
                shortest = dist
                best_action = action





    #print("HERE THEY ARE", actions)go_defensive
    ## IDea for super basic tree: get legal actions, check which is closest to food, choose this.

    '''
    You should change this in your own agent.

    '''

    #util.pause()
    return best_action#random.choice(actions)


class DummyAgent2(CaptureAgent):
  """
  A Dummy agent to serve as an example of the necessary agent structure.
  You should look at baselineTeam.py for more details about how to
  create an agent as this is the bare minimum.
  """

  def registerInitialState(self, gameState):
    """
    This method handles the initial setup of the
    agent to populate useful fields (such as what team
    we're on).

    A distanceCalculator instance caches the maze distances
    between each pair of positions, so your agents can use:
    self.distancer.getDistance(p1, p2)

    IMPORTANT: This method may run for at most 15 seconds.
    """

    '''
    Make sure you do not delete the following line. If you would like to
    use Manhattan distances instead of maze distances in order to save
    on initialization time, please take a look at
    CaptureAgent.registerInitialState in captureAgents.py.
    '''
    CaptureAgent.registerInitialState(self, gameState)

    '''
    Your initialization code goes here, if you need any.

    '''

    enemy_state = look_for_enemies(0,0,gameState, self) # 
    food_state = look_for_food(0,0,gameState, self)
    capsule_state = get_to_tha_capsule(0,0,gameState, self)
    self.decision_tree = Select([enemy_state, capsule_state, food_state]) # The order is important here


  def getSuccessor(self, gameState, action):
    """
    Finds the next successor which is a grid position (location tuple).
    """
    
    successor = gameState.generateSuccessor(self.index, action)
    pos = successor.getAgentState(self.index).getPosition()
    if pos != nearestPoint(pos):
      # Only half a grid position was covered
      return successor.generateSuccessor(self.index, action)
    else:
      return successor

  def chooseAction(self, gameState): # We must run the tree in here each call

    #state = look_for_enemies(gameState,self)
    #found, movement = state.is_enemy_close(gameState, self)
    

    # REMEMBER THAT 
    # SUCCESS = 0
    # FAIL = 1
    # RUN = 2

    global_gamestate.refresh_gameState(gameState)

#### well this seems to be working fine
    enemy_state = look_for_enemies(0,0,gameState, self) # If True then this returns a tuple, state(1) should be the action
    food_state = look_for_food(0,0,gameState, self)
    capsule_state = get_to_tha_capsule(0,0,gameState, self)
    self.decision_tree = Select([enemy_state, capsule_state, food_state]) # The order is important here, it 
###  Probably since the Behavior trees is produced as an iterator and can only be run once
    
    move = 0
    for status in self.decision_tree.iterator:
        move = status 
        #print move, "IS THIS SOMETHING"  
        if status[0] == 0: 
            #print "asso vafon"
            return status[1]
        #else:
        #            move = status 
        #print move, "IS THIS SOMETHING"
        #    return self.chooseAction22(gameState)

        move = status 
        #print move, "IS THIS SOMETHING"
    self.decision_tree.reset
    #print "naha, det blev random"
    return self.chooseAction22(gameState)
    # if found:
    #     #util.pause()
    #     print "fuk shit"
    #     return movement
    #else:
    #    print "JAHAJA"
        #util.pause()
    #    return self.chooseAction22(gameState)#random.choice(actions)

  def chooseAction22(self, gameState):
    """
    Picks among actions randomly.
    """
    actions = gameState.getLegalActions(self.index)
    # Returns the food you're meant to eat. This is in the form of a matrix
    # where m[x][y]=true if there is food you can eat (based on your team) in that square.
    foods = self.getFood(gameState).asList()
    shortest = 9999
    best_action = []
    #foods = foods[:3]
    for food in foods:
        for action in actions:
            successor = self.getSuccessor(gameState, action)
            pos2 = successor.getAgentPosition(self.index)
            dist = self.getMazeDistance(pos2,food)
            if dist < shortest:
                shortest = dist
                best_action = action





    #print("HERE THEY ARE", actions)go_defensive
    ## IDea for super basic tree: get legal actions, check which is closest to food, choose this.

    '''
    You should change this in your own agent.

    '''

    #util.pause()
    return best_action#random.choice(actions)


#class go_defensive(Act):
#    def __init__(self, children=[], name="go_defensive", *args, **kwargs):
#        super(go_defensive, self).__init__(children, name, *args, **kwargs)

class Act(object):
    """ An act is a node in a behavior tree that uses coroutines in its tick function """

    def __init__(self, children = [], name = "", *args, **kwargs):
        self.name = name
        self.iterator = self.tick()
        if children is None:
            children = []

        self.children = children

    def tick(self):
        pass

    def reset(self):
        self.iterator = self.tick()
        for child in self.children:
            child.reset()

    def add_child(self, child):
        self.children.append(child)

    def remove_child(self, child):
        self.children.remove(child)

    def suspend(self):
        pass

    def resume(self):
        pass

    def __enter__(self):
        return self.name;

    def __exit__(self, exc_type, exc_val, exc_tb):
        if exc_type is not None:
            return False
        return True


class get_to_tha_capsule(Act):
    def __init__(self, children=[], name="get_to_tha_capsule", *args, **kwargs):
        super(get_to_tha_capsule, self).__init__(children, name, *args, **kwargs)
        self.current_agent = args[1] #curr_agent

    def tick(self):
        # Is there a capsule on the game field, then rush for it! 
        caps_pos = self.current_agent.getCapsules(global_gamestate.gameState)
        if len(caps_pos)!=0:
            actions = global_gamestate.gameState.getLegalActions(self.current_agent.index)
            shortest = 9999
            best_action = []
            for action in actions:
                successor = self.current_agent.getSuccessor(global_gamestate.gameState, action)
                pos2 = successor.getAgentPosition(self.current_agent.index)
                #print "pos2", pos2, "capspos", caps_pos[0]
                dist = self.current_agent.getMazeDistance(pos2,caps_pos[0])
                if dist > 8:
                    yield ActStatus.FAIL, 1
                if dist < shortest:
                    shortest = dist
                    best_action = action

            #print best_action, "is this uru"

            yield ActStatus.SUCCESS, best_action
        else:
            yield ActStatus.FAIL, 1



class look_for_food(Act):
    def __init__(self, children=[], name="look_for_food", *args, **kwargs):
        super(look_for_food, self).__init__(children, name, *args, **kwargs)
        self.current_agent = args[1] #curr_agent
        #print "FANN", self.current_agent
        #self.look_for_food = look_for_food(self.current_agent.index)
        #print "Tja FUR FAN", self.current_agent  

    def tick(self):

        actions = global_gamestate.gameState.getLegalActions(self.current_agent.index)
        # Returns the food you're meant to eat. This is in the form of a matrix
        # where m[x][y]=true if there is food you can eat (based on your team) in that square.
        foods = self.current_agent.getFood(global_gamestate.gameState).asList()
        shortest = 9999
        best_action = []
        #foods = foods[:3]
        for food in foods:
            for action in actions:
                successor = self.current_agent.getSuccessor(global_gamestate.gameState, action)
                pos2 = successor.getAgentPosition(self.current_agent.index)
                dist = self.current_agent.getMazeDistance(pos2,food)
                if dist < shortest:
                    shortest = dist
                    best_action = action

        #print best_action, "is this uru"

        yield ActStatus.SUCCESS, best_action



class look_for_enemies(Act):
    def __init__(self, children=[], name="look_for_enemies", *args, **kwargs):
        super(look_for_enemies, self).__init__(children, name, *args, **kwargs)
        self.current_agent = args[1] #curr_agent
        #print "FANN", self.current_agent
        self.look_for_foes = look_for_enemies_for_agents(self.current_agent.index)
        #print "Tja FUR FAN", self.current_agent  

    def tick(self):
        #state = look_for_enemies(gameState,self) 
        found, movement = self.look_for_foes.is_enemy_close(global_gamestate.gameState, self.current_agent)
        # Self must be the agent class NOTT THE CURRENT ONEE!!!!
        if found:
            #util.pause()
            #print "fuk shit"
            yield ActStatus.SUCCESS, movement # Don't really need to use the yield keyword, if found then we go for it
        else:
            #print "JAHAJA"
            #util.pause()
            #return self.chooseAction22(gameState)#random.choice(actions)
            yield ActStatus.FAIL, movement

class look_for_enemies_for_agents(CaptureAgent):
    # Checks for enemies within the field of vision
    # Attacks if in defending mode, otherwise runs away
    def is_enemy_close(self, gameState, real_agent):
        enemy_indx = real_agent.getOpponents(gameState)
        
        enemy_seen = []
        for enemy in enemy_indx:
            enemy_seen.append(gameState.getAgentPosition(enemy))
        #print enemy_seen # This is none if we can't clearly see the opponents

        found_one = []
        for enemy in enemy_seen:    
            if enemy is None: #Returns a location tuple if the agent with the given index is observable; if the agent is unobservable, returns None.
                continue
            else:   # Then the enemy is nearby!
                found_one = enemy
                
                break
        if not found_one:
            return False, 1 # filler return
        actions = gameState.getLegalActions(real_agent.index)
        #print actions, "lil"
        #successor = real_agent.getSuccessor(gameState, actions[0]) # Can't have this
        myState = gameState.getAgentState(real_agent.index)#successor.getAgentState(real_agent.index)
        myPos = myState.getPosition()

        if myState.isPacman:
            return True, self.chooseAction_flee(gameState, found_one, real_agent)

        else:
            return True, self.chooseAction_attack(gameState, found_one, real_agent)  

    def getSuccessor(self, gameState, action):
        """
        Finds the next successor which is a grid position (location tuple).
        """
        successor = gameState.generateSuccessor(self.index, action)
        pos = successor.getAgentState(self.index).getPosition()
        if pos != nearestPoint(pos):
        # Only half a grid position was covered
            return successor.generateSuccessor(self.index, action)
        else:
            return successor

    def chooseAction_attack(self, gameState, enemy_pos, real_agent):
        """
        Attempts to attack the enemy
        """
        actions = gameState.getLegalActions(real_agent.index)
        shortest = 9999
        best_action = []
        for action in actions:
            successor = real_agent.getSuccessor(gameState, action)
            pos2 = successor.getAgentPosition(real_agent.index)
            dist = real_agent.getMazeDistance(pos2,enemy_pos)
            if dist < shortest:
                shortest = dist
                best_action = action


        return best_action#random.choice(actions)


    def chooseAction_flee(self, gameState, enemy_pos, real_agent):
        """
        Determines the position which is furthest from the enemy
        """
        actions = gameState.getLegalActions(real_agent.index)
        longest = -9999
        best_action = []
        for action in actions:
            successor = real_agent.getSuccessor(gameState, action)
            pos2 = successor.getAgentPosition(real_agent.index)
            dist = real_agent.getMazeDistance(pos2,enemy_pos)
            if dist > longest:
                longest = dist
                best_action = action

        return best_action#random.choice(actions)


###################################################
##### FROM HERE ON WE HAVE THE BEHAVIOR TREE ######
###################################################

#!/usr/bin/env python
import string
import inspect


class ActStatus(object):
    """ An enumerator representing the status of an act """
    SUCCESS = 0
    FAIL = 1
    RUN = 2



class Select(Act):
    """
    Runs all of its child acts in sequence until one succeeds.
    """

    def __init__(self, children = [], name = "Select", *args, **kwargs):
        super(Select, self).__init__(children, name, *args, **kwargs)
        self.current_child = None

    def tick(self):
        for child in self.children:
            self.current_child = child
            for status in child.iterator:
                if status == ActStatus.RUN:
                    yield status
                elif status == ActStatus.FAIL:
                    yield ActStatus.RUN
                else:
                    yield status
                    return
        yield ActStatus.FAIL

    def suspend(self):
        if self.current_child:
            self.current_child.suspend()

    def resume(self):
        if self.current_child:
            self.current_child.resume()

    def reset(self):
        self.current_child = None
        Act.reset(self)


class Sequence(Act):
    """
    Runs all of its children in sequence until all succeed
    """

    def __init__(self, children = [], name = "Sequence", *args, **kwargs):
        super(Sequence, self).__init__(children, name, *args, **kwargs)
        self.current_child = None

    def tick(self):
        for child in self.children:
            current_child = child
            for status in child.iterator:
                if status == ActStatus.RUN:
                    yield status
                elif status == ActStatus.FAIL:
                    yield ActStatus.FAIL
                    return
                else:
                    yield ActStatus.RUN

        yield ActStatus.SUCCESS

    def suspend(self):
        if self.current_child:
            self.current_child.suspend()

    def resume(self):
        if self.current_child:
            self.current_child.resume()

    def reset(self):
        self.current_child = None
        Act.reset(self)

class Parallel(Act):
    """
    Runs all of its children in parallel until one succeeds
    """

    def __init__(self, children = [], name = "Parallel", *args, **kwargs):
        super(Parallel, self).__init__(children, name, *args, **kwargs)

    def tick(self):
        num_fails = 0
        while True:
            if num_fails >= len(self.children):
                yield ActStatus.FAIL
                return
            try:
                for child in self.children:
                    status = child.iterator.next()

                    if status == ActStatus.SUCCESS:
                        yield ActStatus.SUCCESS
                        return
                    elif status == ActStatus.FAIL:
                        num_fails += 1

            except StopIteration:
                continue
        yield ActStatus.FAIL

    def suspend(self):
        for child in self.children:
            child.suspend()

    def resume(self):
        for child in self.children:
            child.resume()

class Loop(Act):
    """
    Runs all of its children for some number of iters, regardless of whether one succeeds. If iterations is set to
    -1, loops indefinitely. If any child fails, returns FAIL.
    """

    def __init__(self, children = [], name = "Loop", num_iter = -1, *args, **kwargs):
        super(Loop, self).__init__(children, name, *args, **kwargs)
        self.num_iter = num_iter
        self.iter = 1
        self.current_child = None

    def tick(self):
        while True:
            if self.num_iter != -1 and self.iter >= self.num_iter:
                yield ActStatus.SUCCESS
                return

            for child in self.children:
                current_child = child
                for status in child.iterator:
                    if status == ActStatus.RUN:
                        yield status
                    elif status == ActStatus.SUCCESS:
                        yield ActStatus.RUN
                    elif status == ActStatus.FAIL:
                        yield ActStatus.FAIL
                        return
                child.reset()
            self.iter += 1
        yield ActStatus.SUCCESS

    def suspend(self):
        if self.current_child:
            self.current_child.suspend()

    def resume(self):
        if self.current_child:
            self.current_child.resume()


class IgnoreFail(Act):
    """
        Always return either RUNNING or SUCCESS.
    """

    def __init__(self, children = [], name = "IgnoreFail", *args, **kwargs):
        super(IgnoreFail, self).__init__(children, name, *args, **kwargs)
        self.current_child = None

    def tick(self):
        for child in self.children:
            for status in child.iterator:
                if status == ActStatus.FAIL:
                    yield ActStatus.SUCCESS
                else:
                    yield status
        yield ActStatus.SUCCESS

    def suspend(self):
        if self.current_child:
            self.current_child.suspend()

    def resume(self):
        if self.current_child:
            self.current_child.resume()


class Not(Act):
    """
        Returns FAIL on SUCCESS, and SUCCESS on FAIL
    """

    def __init__(self, children = [], name = "Not", *args, **kwargs):
        super(Not, self).__init__(children, name, *args, **kwargs)
        self.current_child = None

    def tick(self):
        for child in self.children:
            current_child = child
            for status in child.iterator:
                if status == ActStatus.FAIL:
                    yield ActStatus.SUCCESS
                elif status == ActStatus.SUCCESS:
                    yield ActStatus.FAIL
                else:
                    yield status
        yield ActStatus.SUCCESS

    def reset(self):
        self.current_child = None
        Act.reset(self)

    def suspend(self):
        if self.current_child:
            self.current_child.suspend()

    def resume(self):
        if self.current_child:
            self.current_child.resume()


class Wrap(Act):
    """
    Wraps a function that returns FAIL or SUCCESS
    """

    def __init__(self, wrap_function, children = [], name = "", *args, **kwargs):
        super(Wrap, self).__init__(children, name, *args, **kwargs)
        self.fn = wrap_function
        self.name = "Wrap " + inspect.getsource(self.fn)

    def tick(self):
        yield self.fn()


def print_act_tree(printTree, indent = 0):
    """
    Print the ASCII representation of an act tree.
    :param tree: The root of an act tree
    :param indent: the number of characters to indent the tree
    :return: nothing
    """
    for child in printTree.children:
        print "    " * indent, "-->", child.name

        if child.children != []:
            print_act_tree(child, indent + 1)


def printobj(text):
    print text
    return ActStatus.SUCCESS

class Count(Act):
    def __init__(self, children=[], name="Count", *args, **kwargs):
        super(Count, self).__init__(children, name, *args, **kwargs)
    
    def tick(self):
        for i in range(1, 10):
            print i
            # Since we use 'yield' here, the loop does not block the program.
            # yield saves the state of the function so that when it is called again,
            # the program returns to this line, rather than the beginning of the
            # function.
            yield ActStatus.RUN

        yield ActStatus.SUCCESS


# class go_agent(Select):
#     # This seems to be working
#     def __init__(self, children=[], name="go_agent", *args, **kwargs):
#         super(go_agent, self).__init__(children, name, *args, **kwargs)

#     def tick(self):
#         if points > 200:
#             yield ActStatus.SUCCESS
#             for child in self.children:
#                 self.current_child = child
#                 for status in child.iterator:
#                     if status == ActStatus.RUN:
#                         yield status
#                     elif status == ActStatus.FAIL:
#                         yield ActStatus.RUN
#                     else:
#                         yield status
#                         return
#             yield ActStatus.FAIL

#         else:
#             yield ActStatus.FAIL



# if __name__ == "__main__": # Just in place of a "main()" function, a bit different but essentially the same
#     # tree = Parallel(
#     #     [
#     #         Loop(
#     #             [
#     #                 Wrap(lambda: printobj("Hello 1")),
#     #                 Wrap(lambda: printobj("Hello 2"))
#     #             ], num_iter=10),
#     #         Loop(
#     #             [
#     #                 Wrap(lambda: printobj("Hello 3")),
#     #                 Wrap(lambda: printobj("Hello 4"))
#     #             ], num_iter=5),
#     #     ]
#     # )
#     # print_act_tree(tree)

#     # tree.reset()


#     for status in tree.iterator:
#         pass







