
import math
import random
import numpy as np
from numpy.linalg import inv


class ParticleFilter2D:
    def __init__(self, num_particles=1000, dt=0.1):
        self.dt = dt
        self.particle_states = np.zeros( (3, num_particles) )
        self.particle_weights = np.zeros( num_particles )
        self.num_particles = num_particles
        self.filter_params = {}



    def set_filter_psarametersLAB2(self):
        self.filter_params['Q'] = np.matrix( [[10, 0],
                                              [0, 10]] )
        self.filter_params['Q'] = self.filter_params['Q'] * 10

        self.filter_params['R'] = np.matrix( [[2, 0, 0],
                                             [0, 2, 0],
                                             [0, 0, 1.01]] )
        self.filter_params['R'] = self.filter_params['R'] * 100

        self.filter_params['v_0'] = (2.0 * np.pi * 200.0) / 688
        self.filter_params['theta_0'] = 0.0
        self.filter_params['omega_0'] = 0.003
        self.filter_params['thresh_avg_likelihood'] = 0.00001
        self.filter_params['state_space_bounds'] = np.matrix( [[0, 0, -np.pi],
                        
                                                             
                                                             [640, 480, np.pi]] )


    def generate_initial_belief(self):
        self.particle_states[0,...] = np.random.rand(self.num_particles) * self.filter_params['state_space_bounds'][1,0]
        self.particle_states[1,...] = np.random.rand(self.num_particles) * self.filter_params['state_space_bounds'][1,1]
        self.particle_states[2,...] = np.random.rand(self.num_particles) * 2 * np.pi - np.pi
        self.particle_weights = np.ones(self.num_particles) * (1/self.num_particles)




    def predict(self):
        u = np.ones( (3, self.num_particles) )
        u[0,...] = self.filter_params['v_0'] * np.cos(self.particle_states[2,...])
        u[1,...] = self.filter_params['v_0'] * np.sin(self.particle_states[2,...])
        u[2,...] = self.filter_params['omega_0'] * np.ones( self.num_particles )
        
        signs = [-1, 1]
        stdv_process = np.sqrt( self.filter_params['R'] )
        disturbances = np.random.rand(3, self.num_particles)
        disturbances[0,...] = disturbances[0,...] * stdv_process[0,0] * random.choice(signs)
        disturbances[1,...] = disturbances[1,...] * stdv_process[1,1] * random.choice(signs)
        disturbances[2,...] = disturbances[2,...] * stdv_process[2,2] * random.choice(signs)
        
        self.particle_states = self.particle_states + self.dt * u + disturbances
        
        




    def calculate_weights(self, i, z_i):
        D = np.zeros(self.num_particles)
        prob = np.zeros(self.num_particles)
        inv_Q = inv(self.filter_params['Q'])
        for m in range(0, self.num_particles):
            D[m] = inv_Q[0,0] * ( z_i[0] - self.particle_states[0,m] )**2 + inv_Q[1,1] * (z_i[1] - self.particle_states[1,m])**2
            
        prob = np.exp(-0.5 * D)

        if(np.mean(prob) < self.filter_params['thresh_avg_likelihood']):
            print("Measurement ", i, " is an outlier.")
            prob = np.ones(self.num_particles)
            
        prob_sum = np.sum(prob)
        self.particle_weights = prob.copy()
        if(prob_sum != 0):
            self.particle_weights /= prob_sum
            
        



    def systematic_resample(self, random_seed):
        cdf = np.cumsum(self.particle_weights)
        new_particle_set = np.zeros( (3, self.num_particles) )
        np.random.seed(random_seed)

        r_0 = np.random.rand() * (1/self.num_particles)

        for m in range(0, self.num_particles):
            idx = np.where(cdf >= r_0)[0][0]
            new_particle_set[...,m] = self.particle_states[...,idx].copy()
            r_0 += (1 / self.num_particles)

        self.particle_states = new_particle_set.copy()
        self.particle_weights = np.ones( self.num_particles ) * (1 / self.num_particles)





    def plot_distribution(self, x_i, z_i, fig, ax, color='g'):
        ax.set_xlim( (self.filter_params['state_space_bounds'][0,0], self.filter_params['state_space_bounds'][1,0]) )
        ax.set_ylim( (self.filter_params['state_space_bounds'][0,1], self.filter_params['state_space_bounds'][1,1]) )
        ax.scatter(self.particle_states[0,...], self.particle_states[1,...], c=color)
        ax.scatter(z_i[0], z_i[1], c='r')
        ax.scatter(x_i[0], x_i[1], c='b')

