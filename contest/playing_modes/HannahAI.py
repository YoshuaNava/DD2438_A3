# baselineTeam.py
# ---------------
# Licensing Information:  You are free to use or extend these projects for
# educational purposes provided that (1) you do not distribute or publish
# solutions, (2) you retain this notice, and (3) you provide clear
# attribution to UC Berkeley, including a link to http://ai.berkeley.edu.
#
# Attribution Information: The Pacman AI projects were developed at UC Berkeley.
# The core projects and autograders were primarily created by John DeNero
# (denero@cs.berkeley.edu) and Dan Klein (klein@cs.berkeley.edu).
# Student side autograding was added by Brad Miller, Nick Hay, and
# Pieter Abbeel (pabbeel@cs.berkeley.edu).


# baselineTeam.py
# ---------------
# Licensing Information: Please do not distribute or publish solutions to this
# project. You are free to use and extend these projects for educational
# purposes. The Pacman AI projects were developed at UC Berkeley, primarily by
# John DeNero (denero@cs.berkeley.edu) and Dan Klein (klein@cs.berkeley.edu).
# For more info, see http://inst.eecs.berkeley.edu/~cs188/sp09/pacman.html

from captureAgents import CaptureAgent
import distanceCalculator
import random, time, util, sys
from game import Directions
import game
from util import nearestPoint

#################
# Team creation #
#################

def createTeam(firstIndex, secondIndex, isRed,
               first = 'OffensiveReflexAgent', second = 'DoNothingAgent'):
  """
  This function should return a list of two agents that will form the
  team, initialized using firstIndex and secondIndex as their agent
  index numbers.  isRed is True if the red team is being created, and
  will be False if the blue team is being created.

  As a potentially helpful development aid, this function can take
  additional string-valued keyword arguments ("first" and "second" are
  such arguments in the case of this function), which will come from
  the --redOpts and --blueOpts command-line arguments to capture.py.
  For the nightly contest, however, your team will be created without
  any extra arguments, so you should make sure that the default
  behavior is what you want for the nightly contest.
  """
  #if(True):
    #    first = 'OffensiveReflexAgent'
  return [eval(first)(firstIndex), eval(second)(secondIndex)]





##########
# Agents #
##########

class ReflexCaptureAgent(CaptureAgent):
  """
  A base class for reflex agents that chooses score-maximizing actions
  """



  def registerInitialState(self, gameState):
    self.start = gameState.getAgentPosition(self.index)
    CaptureAgent.registerInitialState(self, gameState)

    # get distance in manhattan distance
    self.distancer = distanceCalculator.Distancer(gameState.data.layout)
    self.distancer.getMazeDistances()

    # MCTS parameters
    self.maxLength = 10   # max search length
    self.maxTime = 10     # max time (1s) TODO: implement timer and return best option when time is up
    self.maxActions = 10


    self.C = 1.4
    self.stats = {}
    self.plays = {}
    self.wins = {}



  """
  Picks among the actions with the highest Q(s,a).
  """
  def chooseAction(self, gameState):
    """
    Pick up a random (feasible) action, just in case MCTS needs to be stopped before giving a good result
    """
    actions = gameState.getLegalActions(self.index)
    bestAction = random.choice(actions)
#    print(actions)

    currentState = gameState.getAgentState(self.index)

    if(True):#gameState.getAgentState(self.index).isPacman):

        """Monte Carlo Tree Simulation"""
        root = Node(gameState)
        root.parent=None
        root.children = []
        t = 0

        while(t < self.maxTime):
            run_simulation(self, root, self.maxLength)
            t+=1

        actions = root.state.getLegalActions(self.index)                                # legal actions
        successors = [(p, root.state.generateSuccessor(self.index, p)) for p in actions]


        sorted_list = sorted(
            ((100 * self.wins.get((self.index, S), 0) /
              self.plays.get((self.index, S), 1),
              self.wins.get((self.index, S), 0),
              self.plays.get((self.index, S), 0), p)
             for p, S in successors),
            reverse=True
        )

        print("sorted list")
        print(sorted_list)

        percent_wins, bestAction = (sorted_list[0][0], sorted_list[0][3])

        print("Action to take")
        print(percent_wins)
        print(bestAction)

        for x in sorted_list:
            print "{3}: {0:.2f}% ({1} / {2})".format(*x)


        return bestAction
    else:
        return self.ghostBehavior(gameState, actions)

  def ghostBehavior(self, gameState, actions):
      return 'Stop'



  def getSuccessor(self, gameState, action):
    """
    Finds the next successor which is a grid position (location tuple).
    """
    successor = gameState.generateSuccessor(self.index, action)
    pos = successor.getAgentState(self.index).getPosition()
    if pos != nearestPoint(pos):
      # Only half a grid position was covered
      return successor.generateSuccessor(self.index, action)
    else:
      return successor





  def evaluate(self, gameState, action):
    """
    Computes a linear combination of features and feature weights
    """
    features = self.getFeatures(gameState, action)
    weights = self.getWeights(gameState, action)
    return features * weights




  def getFeatures(self, gameState, action):
    """
    Returns a counter of features for the state
    """
    features = util.Counter()
    successor = self.getSuccessor(gameState, action)
    features['successorScore'] = self.getScore(successor)
    return features




  def getWeights(self, gameState, action):
    """
    Normally, weights do not depend on the gamestate.  They can be either
    a counter or a dictionary.
    """
    return {'successorScore': 1.0}




def MCTS_0(self, node, maxLength):

    # --- find leaf node
    l = 0
    while(len(node.children)>0):
        if(l>=maxLength):
            print("hola")
            return node
        node = random.choice(node.children)
        l += 1

    # --- expand node
    actions = node.state.getLegalActions(self.index)                                # legal actions

    successors = [(p, node.state.generateSuccessor(self.index, p)) for p in actions]
#    print(successors)

    # --- add new leaf nodes to tree
    for successor in successors:
#        childState = node.state.generateSuccessor(self.index, actions[i])
        childState = successor[1]
        childNode = Node(childState)
        childNode.action = successor[0]
        childNode.children = []
        childNode.points = node.points + getPoints(self, childState, "eatFood")

        node.children.append(childNode)


    # --- find best action
    minPoints = float('inf')
    for i in range(len(node.children)):
        if(node.children[i].points < minPoints):
            minPoints = node.children[i].points
            bestAction = node.children[i].action

    return bestAction, minPoints


import math

def run_simulation(self, node, maxLength):

    visited_states = set()

    print("hej")
    expand = True
    state = node.state

    #test
    enemies = [state.getAgentState(i) for i in self.getOpponents(state)]
    invaders = [a for a in enemies if a.isPacman and a.getPosition() != None]

    #if no invaders
    print("---------------------")
    chasePacman = False
    if(len(invaders)>0):
        chasePacman = False
        distToInvaders_prev = 0
        for i in range(len(invaders)):
            #print(invaders[i].getPosition())
            distToInvaders_prev += self.distancer.getDistance(state.getAgentState(self.index).getPosition(), invaders[i].getPosition())
    print("---------------------")
    print(invaders)

    foodLeft_prev = len(self.getFoodYouAreDefending(state).asList())

    for i in xrange(maxLength):
        legalActions = state.getLegalActions(self.index)
        successors = [(p, state.generateSuccessor(self.index, p)) for p in legalActions]

        if all(self.plays.get((self.index, S)) for p, S in successors):
            """
            UCB1 formula for selecting the best action to take from an already visited state
            """
            log_total = math.log(
                    sum(self.plays[(self.index, S)] for p, S in successors))
            value, action, state = max(
                    ((self.wins[(self.index, S)] / self.plays[(self.index, S)]) +
                     self.C * math.sqrt(log_total / self.plays[(self.index, S)]), p, S)
                    for p, S in successors
                    )
        else:
            """
            If there is not enough information for the UCB1 formula, take a random action
            """
            action, state = random.choice(successors)


        if(expand == True) and ((self.index, state) not in self.plays):
            print("Expansion")
            self.plays[ (self.index, state) ] = 0
            self.wins[ (self.index, state) ] = 0
            expand = False


        visited_states.add( (self.index, state) )

        foodLeft = len(self.getFoodYouAreDefending(state).asList())
        print("Iteration",i)
        print(action)
        print(foodLeft)
        print(foodLeft_prev)

        enemies = [state.getAgentState(i) for i in self.getOpponents(state)]
        invaders = [a for a in enemies if a.isPacman and a.getPosition() != None]
        distToInvaders = 0
        if(chasePacman):
            for i in range(len(invaders)):
                distToInvaders_prev += self.distancer.getDistance(state.getAgentState(self.index).getPosition(), invaders[i].getPosition())

        if(self.index, state) in self.plays:
            self.plays[(self.index, state)] += 1

            if(chasePacman):
                if(distToInvaders < distToInvaders_prev):
                    self.wins[(self.index, state)] += 1

            else:
                if(foodLeft < foodLeft_prev):
                    self.wins[(self.index, state)] += 1


    #print(legalActions)
    #print(rand_action)
    #print(self.plays)
    #print(self.wins)




def getPoints(self, state, tactics):
    points = 0
    if(tactics=="eatFood"):
        food = self.getFood(state)
        for i in range(food.width):
            for j in range(food.height):
                if(food[i][j]):
                    points += self.distancer.getDistance(state.getAgentState(self.index).getPosition(), (i,j))
    return points



class Node():
    def __init__(self, state=None, points=0, children=[], action=None):
        self.state = state
        self.points = points
        self.children = children
        self.action = action

    def __str__(self):
        return ("points:" + str(self.points))



class OffensiveReflexAgent(ReflexCaptureAgent):
  """
  A reflex agent that seeks food. This is an agent
  we give you to get an idea of what an offensive agent might look like,
  but it is by no means the best or only way to build an offensive agent.
  """


  def getFeatures(self, gameState, action):
    features = util.Counter()
    successor = self.getSuccessor(gameState, action)
    foodList = self.getFood(successor).asList()
    features['successorScore'] = -len(foodList)#self.getScore(successor)

    # Compute distance to the nearest food

    if len(foodList) > 0: # This should always be True,  but better safe than sorry
      myPos = successor.getAgentState(self.index).getPosition()
      minDistance = min([self.getMazeDistance(myPos, food) for food in foodList])
      features['distanceToFood'] = minDistance
    return features



  def getWeights(self, gameState, action):
    return {'successorScore': 100, 'distanceToFood': -1}

  def ghostBehavior(self, gameState, actions):
      ''' Offensive - go to other side '''
      # You can profile your evaluation time by uncommenting these lines
      #start = time.time()
      values = [self.evaluate(gameState, a) for a in actions]
      #print 'eval time for agent %d: %.4f' % (self.index, time.time() - start)

      maxValue = max(values)
      bestActions = [a for a, v in zip(actions, values) if v == maxValue]
      print("Best actions", bestActions)

      return random.choice(bestActions)

class DefensiveReflexAgent(ReflexCaptureAgent):
  """
  A reflex agent that keeps its side Pacman-free. Again,
  this is to give you an idea of what a defensive agent
  could be like.  It is not the best or only way to make
  such an agent.
  """

  def getFeatures(self, gameState, action):
    features = util.Counter()
    successor = self.getSuccessor(gameState, action)

    myState = successor.getAgentState(self.index)
    myPos = myState.getPosition()

    # Computes whether we're on defense (1) or offense (0)
    features['onDefense'] = 1
    if myState.isPacman: features['onDefense'] = 0

    # Computes distance to invaders we can see
    enemies = [successor.getAgentState(i) for i in self.getOpponents(successor)]
    invaders = [a for a in enemies if a.isPacman and a.getPosition() != None]
    features['numInvaders'] = len(invaders)
    if len(invaders) > 0:
      dists = [self.getMazeDistance(myPos, a.getPosition()) for a in invaders]
      features['invaderDistance'] = min(dists)

    if action == Directions.STOP: features['stop'] = 1
    rev = Directions.REVERSE[gameState.getAgentState(self.index).configuration.direction]
    if action == rev: features['reverse'] = 1

    return features

  def getWeights(self, gameState, action):
    return {'numInvaders': -1000, 'onDefense': 100, 'invaderDistance': -10, 'stop': -100, 'reverse': -2}

  def ghostBehavior(self, gameState, actions):
    ''' Defensive - keep own side free from pacman '''
    # You can profile your evaluation time by uncommenting these lines
    #start = time.time()
    values = [self.evaluate(gameState, a) for a in actions]
    #print 'eval time for agent %d: %.4f' % (self.index, time.time() - start)

    maxValue = max(values)
    bestActions = [a for a, v in zip(actions, values) if v == maxValue]
    print("Best actions", bestActions)

    return random.choice(bestActions)


class DoNothingAgent(CaptureAgent):
  """
  A Dummy agent to serve as an example of the necessary agent structure.
  You should look at baselineTeam.py for more details about how to
  create an agent as this is the bare minimum.
  """

  def registerInitialState(self, gameState):
    """
    This method handles the initial setup of the
    agent to populate useful fields (such as what team
    we're on).

    A distanceCalculator instance caches the maze distances
    between each pair of positions, so your agents can use:
    self.distancer.getDistance(p1, p2)

    IMPORTANT: This method may run for at most 15 seconds.
    """

    '''
    Make sure you do not delete the following line. If you would like to
    use Manhattan distances instead of maze distances in order to save
    on initialization time, please take a look at
    CaptureAgent.registerInitialState in captureAgents.py.
    '''
    CaptureAgent.registerInitialState(self, gameState)

    '''
    Your initialization code goes here, if you need any.
    '''


  def chooseAction(self, gameState):
    '''
    You should change this in your own agent.
    '''
    return "Stop"
