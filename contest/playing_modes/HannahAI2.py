# baselineTeam.py
# ---------------
# Licensing Information:  You are free to use or extend these projects for
# educational purposes provided that (1) you do not distribute or publish
# solutions, (2) you retain this notice, and (3) you provide clear
# attribution to UC Berkeley, including a link to http://ai.berkeley.edu.
#
# Attribution Information: The Pacman AI projects were developed at UC Berkeley.
# The core projects and autograders were primarily created by John DeNero
# (denero@cs.berkeley.edu) and Dan Klein (klein@cs.berkeley.edu).
# Student side autograding was added by Brad Miller, Nick Hay, and
# Pieter Abbeel (pabbeel@cs.berkeley.edu).


# baselineTeam.py
# ---------------
# Licensing Information: Please do not distribute or publish solutions to this
# project. You are free to use and extend these projects for educational
# purposes. The Pacman AI projects were developed at UC Berkeley, primarily by
# John DeNero (denero@cs.berkeley.edu) and Dan Klein (klein@cs.berkeley.edu).
# For more info, see http://inst.eecs.berkeley.edu/~cs188/sp09/pacman.html

from captureAgents import CaptureAgent
import distanceCalculator
import random, time, util, sys
from game import Directions
import game
from util import nearestPoint
import math


#################
# Team creation #
#################

def createTeam(firstIndex, secondIndex, isRed,
               #first = 'OffensiveAgent', second = 'DefensiveAgent'):
               #first = 'DefensiveAgent', second = 'DoNothingAgent'):
               first = 'OffensiveAgent', second = 'DoNothingAgent'):
  """
  This function should return a list of two agents that will form the
  team, initialized using firstIndex and secondIndex as their agent
  index numbers.  isRed is True if the red team is being created, and
  will be False if the blue team is being created.

  As a potentially helpful development aid, this function can take
  additional string-valued keyword arguments ("first" and "second" are
  such arguments in the case of this function), which will come from
  the --redOpts and --blueOpts command-line arguments to capture.py.
  For the nightly contest, however, your team will be created without
  any extra arguments, so you should make sure that the default
  behavior is what you want for the nightly contest.
  """
  return [eval(first)(firstIndex), eval(second)(secondIndex)]

##########
# Agents #
##########

class ReflexCaptureAgent(CaptureAgent):
  """
  A base class for reflex agents that chooses score-maximizing actions
  """
  def registerInitialState(self, gameState):
    self.start = gameState.getAgentPosition(self.index)
    CaptureAgent.registerInitialState(self, gameState)

    self.distancer = distanceCalculator.Distancer(gameState.data.layout)
    self.distancer.getMazeDistances()

    # MCTS parameters
    self.maxLength = 5   # max search length
    self.maxTime = 25     # max time (1s) TODO: implement timer and return best option when time is up
    self.maxActions = 1200

    self.C = 1.4
    self.plays = {}
    self.wins = {}

    # Debug colors
    self.Red = (1,0.5,0.5)
    self.Green = (0.5,1,0.5)
    self.Blue = (0.5,0.5,1)

  """
  """
  def chooseAction(self, gameState):
    """
    Pick up a random (feasible) action, just in case MCTS needs to be stopped before giving a good result
    """
    print("-------------------------")

    actions = gameState.getLegalActions(self.index)
    bestAction = random.choice(actions)

    print str(len(actions)) + " legal actions:"
    print actions

    # if only one action - return it
    if len(actions) == 1:
        return bestAction

    self.plays = {}
    self.wins = {}

    """Monte Carlo Tree Simulation"""
    t = 0
    while(t < self.maxTime):
        CaptureAgent.debugClear(self)
        run_simulation(self, gameState, self.maxLength)
        t+=1

    actions = gameState.getLegalActions(self.index)
    successors = [(p, self.getSuccessor(gameState, p)) for p in actions]

    sorted_list = sorted(
        ((100 * self.wins.get((self.index, S), 0) /
          self.plays.get((self.index, S), 1),
          self.wins.get((self.index, S), 0),
          self.plays.get((self.index, S), 0), p)
         for p, S in successors),
        reverse=True
    )

    print("sorted list")
    print(sorted_list)

    percent_wins, bestAction = (sorted_list[0][0], sorted_list[0][3])

    print("Action to take")
    print(percent_wins)
    print(bestAction)

    for x in sorted_list:
        print "{3}: {0:.2f}% ({1} / {2})".format(*x)

    # debug draw best action
    bestState = self.getSuccessor(gameState, bestAction).getAgentState(self.index)
    CaptureAgent.debugDraw(self, bestState.getPosition(), self.Green, False)

    return bestAction

  def getSuccessor(self, gameState, action):
    """
    Finds the next successor which is a grid position (location tuple).
    """
    successor = gameState.generateSuccessor(self.index, action)
    pos = successor.getAgentState(self.index).getPosition()
    if pos != nearestPoint(pos):
      # Only half a grid position was covered
      return successor.generateSuccessor(self.index, action)
    else:
      return successor

  def distToGoal(self, state):
      # if pacman, return distance to start
      if(state.getAgentState(self.index).isPacman):
          return self.getMazeDistance(self.start, state.getAgentState(self.index).getPosition())

      # if ghost, return distance to enemy start
      enemies = [state.getAgentState(i) for i in self.getOpponents(state)]
      enemyStart = enemies[0].start.getPosition()
      return self.getMazeDistance(enemyStart, state.getAgentState(self.index).getPosition())

  def getPoints(self, state):
      return 0

def run_simulation(self, start_state, maxLength):

    visited_states = set()

    expand = True
    state = start_state
    foodLeft_prev = self.getPoints(state)
    pos_prev = state.getAgentPosition(self.index)
    dist_start_prev = self.distToGoal(state)

    for i in xrange(maxLength):
        legalActions = state.getLegalActions(self.index)
        successors = [(p, self.getSuccessor(state, p)) for p in legalActions]

        if all(self.plays.get((self.index, S)) for p, S in successors):
            """
            UCB1 formula for selecting the best action to take from an already visited state
            """
            log_total = math.log(
                    sum(self.plays[(self.index, S)] for p, S in successors))

            nodes_list = sorted( (
                    ((self.wins[(self.index, S)] / self.plays[(self.index, S)]) +
                     self.C * math.sqrt(log_total / self.plays[(self.index, S)]), p, S)
                    for p, S in successors),
                    reverse=True
                    )
            value, action, state = (nodes_list[0][0], nodes_list[0][1], nodes_list[0][2])
        else:
            """
            If there is not enough information for the UCB1 formula, take a random action
            """
            action, state = random.choice(successors)

        visited_states.add( (self.index, state) )

        if(expand == True) and ((self.index, state) not in self.plays):
            #print("Expansion")
            self.plays[ (self.index, state) ] = 0
            self.wins[ (self.index, state) ] = 0
            expand = False

        CaptureAgent.debugDraw(self, state.getAgentState(self.index).getPosition(), self.Blue, False)

        foodLeft = self.getPoints(state)
        #print("Iteration",i)
        #print("pos: " + str(state.getAgentState(self.index).getPosition()))
        #print(action)
        #print(foodLeft)
        #print(foodLeft_prev)

        if(self.index, state) in self.plays:
            self.plays[(self.index, state)] += 1
            if(foodLeft_prev > 0):
                if(foodLeft < foodLeft_prev):
                    self.wins[(self.index, state)] += 1
            else:
                pos_new = state.getAgentPosition(self.index)
                dist_start_new =  self.distToGoal(state)
                if(dist_start_new < dist_start_prev):
                    self.wins[(self.index, state)] += 1

class OffensiveAgent(ReflexCaptureAgent):
  """
  An agent that seeks food.
  """

  def getPoints(self, state):
    points = 0
    pos = state.getAgentState(self.index).getPosition()
    food = self.getFood(state)

    if(not state.getAgentState(self.index).isPacman):
        enemies = [state.getAgentState(i) for i in self.getOpponents(state)]
        invaders = [a for a in enemies if a.isPacman and a.getPosition() != None]
        if len(invaders) >0:
            points += min([self.getMazeDistance(pos, i.getPosition()) for i in invaders])

    # TODO: add else: max distance to ghosts
    points += min([self.getMazeDistance(pos, f) for f in food.asList()])
    foodList = food.asList()
    return points

class DefensiveAgent(ReflexCaptureAgent):
  """
  An agent that keeps its side Pacman-free.
  """

  def getPoints(self, state):
    points = 0
    pos = state.getAgentState(self.index).getPosition()
    food = []
    if(not state.getAgentState(self.index).isPacman):
        food = self.getFoodYouAreDefending(state)

        enemies = [state.getAgentState(i) for i in self.getOpponents(state)]
        invaders = [a for a in enemies if a.isPacman and a.getPosition() != None]
        if len(invaders)>0:
            points += min([self.getMazeDistance(pos, i.getPosition()) for i in invaders])
        else:
            points +=  min([self.getMazeDistance(pos, f) for f in food.asList()])
    else:
        food = self.getFood(state)
        points +=  min([self.getMazeDistance(pos, f) for f in food.asList()])

        # TODO: add max distance to ghosts

    return points


class DoNothingAgent(CaptureAgent):
  """
  A Dummy agent to serve as an example of the necessary agent structure.
  You should look at baselineTeam.py for more details about how to
  create an agent as this is the bare minimum.
  """

  def registerInitialState(self, gameState):
    """
    This method handles the initial setup of the
    agent to populate useful fields (such as what team
    we're on).

    A distanceCalculator instance caches the maze distances
    between each pair of positions, so your agents can use:
    self.distancer.getDistance(p1, p2)

    IMPORTANT: This method may run for at most 15 seconds.
    """

    '''
    Make sure you do not delete the following line. If you would like to
    use Manhattan distances instead of maze distances in order to save
    on initialization time, please take a look at
    CaptureAgent.registerInitialState in captureAgents.py.
    '''
    CaptureAgent.registerInitialState(self, gameState)

    '''
    Your initialization code goes here, if you need any.
    '''


  def chooseAction(self, gameState):
    '''
    You should change this in your own agent.
    '''
    return "Stop"
